" Backspace behavior
set backspace=indent,eol,start

" Load common settings
source ~/.vim/config/config.vim

" Jellybeans
set t_Co=256
color jellybeans
