inoremap deff def  do<CR>end<Esc>kla
inoremap defm defmodule  do<CR>end<Esc>kela
inoremap defp defp  do<CR>end<Esc>k2la
inoremap fn, fn  end<Esc>bhi
inoremap fnn fn -><CR>end<Esc>O
inoremap doo do<CR>end<Esc>ko
inoremap ## #{}<Esc>i
inoremap testt test  do<CR>end<Esc>kela
inoremap @doc @doc ~S"""<CR>"""<Esc>O
inoremap @moduledoc @doc ~S"""<CR>"""<Esc>O
