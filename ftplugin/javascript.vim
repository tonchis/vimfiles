inoremap log-- console.log();<Esc>hi

" This mapping adds a ES6 () => {} function typically used " as a block in
" many JS functions, leaving you inserting inside the ().
"
" It assumes you're inside a function, so it'll add a `;` at the end of the
" line.
"
" Example:
"
"   array.map((--);
"
" Becomes:
"
"   array.map((<cursor>) => {});
"
inoremap (-- () => {}<Esc>A;<Esc>F(a

" This mapping adds a ES6 () => {} function typically used as a block in
" many JS functions, leaving you inserting inside the {}.
"
" It assumes you're inside a function, so it'll add a `;` at the end of the
" line.
"
" Example:
"
"   it('test', {--)
"
" Becomes:
"
"   it('test', () => {
"     <cursor>
"   });
"
inoremap {-- () => {}<Esc>A;<Esc>F{a<CR><CR><Esc>kA<Tab>
